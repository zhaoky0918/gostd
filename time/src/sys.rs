use gostd_builtin::*;
use std::time::{Instant, SystemTime};
pub fn monotonic_now() -> uint64 {
    // inner::monotonic_now()
    let now = Instant::now();
    now.elapsed().as_secs()
}

pub fn real_time_now() -> (uint64, uint64) {
    // inner::real_time_now()
    let now = SystemTime::now();
    let duration = now.duration_since(std::time::UNIX_EPOCH).unwrap();
    (duration.as_secs(), duration.subsec_nanos() as u64)
}

#[cfg(all(all(unix), not(target_os = "macos")))]
#[path = "sys/unix.rs"]
mod inner;

#[cfg(any(target_os = "macos", target_os = "ios"))]
#[path = "sys/darwin.rs"]
mod inner;
